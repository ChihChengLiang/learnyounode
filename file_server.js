/*
Write an HTTP server that serves the same text file for each request it receives.

Your server should listen on the port provided by the first argument to your program.

You will be provided with the location of the file to serve as the second command-line argument. 
You must use the fs.createReadStream() method to stream the file contents to the response.
*/




var http = require('http');
var fs = require('fs');

args = process.argv;

port = args[2];
location = args[3];

/*
console.log(args);
[ '/usr/bin/nodejs',
  '/home/chihchengliang/learnyounode/file_server.js',
  '45603',
  '/tmp/_learnyounode_10845.txt' ]
*/



var server = http.createServer(function (req, res) {
      // request handling logic...
 //   console.log(req);
//    console.log("<------------->")
//    console.log(res);
     fs.createReadStream(location).pipe(res)
})
server.listen(port);

/*

Here's the official solution in case you want to compare notes:

────────────────────────────────────────────────────────────────────────────────
    var http = require('http')
    var fs = require('fs')
    
    var server = http.createServer(function (req, res) {
      res.writeHead(200, { 'content-type': 'text/plain' })
    
      fs.createReadStream(process.argv[3]).pipe(res)
    })
    
    server.listen(Number(process.argv[2]))

────────────────────────────────────────────────────────────────────────────────
*/


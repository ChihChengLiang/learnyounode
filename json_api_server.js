/*
Write an HTTP server that serves JSON data when it receives a GET request to the path '/api/parsetime'. Expect the request to contain a query string with a key 'iso' and an ISO-format time as the value.

For example:

  /api/parsetime?iso=2013-08-10T12:10:15.474Z

The JSON response should contain only 'hour', 'minute' and 'second' properties. For example:

    {
      "hour": 14,
      "minute": 23,
      "second": 15
    }

Add second endpoint for the path '/api/unixtime' which accepts the same query string but returns UNIX epoch time under the property 'unixtime'. For example:

    { "unixtime": 1376136615474 }

Your server should listen on the port provided by the first argument to your program.
*/
var http = require('http')
var url = require('url')

var server = http.createServer(function (req, res) {
    if (req.method != 'GET')
        return res.end('send me a GET\n')
    parsed_api_call = url.parse(req.url, true);
    d = new Date(parsed_api_call.query.iso);
    var a = new Object();
    switch (parsed_api_call.pathname) {
    case '/api/parsetime':
        /*
        //console.log('/api/parsetime');
        //console.log(parsed_api_call.query)
        /api/parsetime
        { iso: '2014-11-10T07:27:44.995Z' }
*/
        a.hour = d.getHours();
        a.minute = d.getMinutes();
        a.second = d.getSeconds();

        break;
    case '/api/unixtime':
        /*
        //console.log('/api/unixtime');
        //console.log(parsed_api_call.query)
        /api/unixtime
        { iso: '2014-11-10T07:27:44.995Z' }
*/
        a.unixtime = d.getTime();

        break;
    }
    res.writeHead(200, {
        'Content-Type': 'application/json'
    })
    res.end(JSON.stringify(a));

})

server.listen(Number(process.argv[2]))

/*

Here's the official solution in case you want to compare notes:

────────────────────────────────────────────────────────────────────────────────
    var http = require('http')
    var url = require('url')
    
    function parsetime (time) {
      return {
        hour: time.getHours(),
        minute: time.getMinutes(),
        second: time.getSeconds()
      }
    }
    
    function unixtime (time) {
      return { unixtime : time.getTime() }
    }
    
    var server = http.createServer(function (req, res) {
      var parsedUrl = url.parse(req.url, true)
      var time = new Date(parsedUrl.query.iso)
      var result
    
      if (/^\/api\/parsetime/.test(req.url))
        result = parsetime(time)
      else if (/^\/api\/unixtime/.test(req.url))
        result = unixtime(time)
    
      if (result) {
        res.writeHead(200, { 'Content-Type': 'application/json' })
        res.end(JSON.stringify(result))
      } else {
        res.writeHead(404)
        res.end()
      }
    })
    server.listen(Number(process.argv[2]))

────────────────────────────────────────────────────────────────────────────────
*/

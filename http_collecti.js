var http = require('http');
var bl = require('bl');

args = process.argv;

url = args[2]

http.get(url, function (res) {
    res.pipe(bl(function (err, data) {
        console.log(data.length);
        console.log(data.toString());
    }))
});


/* official solution
    var http = require('http')
    var bl = require('bl')
    
    http.get(process.argv[2], function (response) {
      response.pipe(bl(function (err, data) {
        if (err)
          return console.error(err)
        data = data.toString()
        console.log(data.length)
        console.log(data)
      }))  
    })
*/
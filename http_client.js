var http = require('http');
args = process.argv;


/*console.log(args); will yield the following results

[ '/usr/bin/nodejs',
  '/home/chihchengliang/learnyounode/http_client.js',
  'http://localhost:48993' ]*/


url = args[2];

http.get(url, function (res) {
    res.setEncoding("utf8");
    res.on("data", function (data) {
        console.log(data)
    })
})


/*
Official solutions:

var http = require('http')

http.get(process.argv[2], function (response) {
    response.setEncoding('utf8')
    response.on('data', console.log)
    response.on('error', console.error)
})*/
